﻿using System.Threading.Tasks;
using DependencyInjection.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace DependencyInjection.Middleware
{
    public class WeatherMiddleware
    {
        private readonly RequestDelegate next;
    
        public WeatherMiddleware(RequestDelegate nextDelegate)
        {
            next = nextDelegate;
        }
    
        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value == "/middleware/class")
            {
                await context.Response.WriteAsync("Middleware Class: It is raining in London");
            }
            else
            {
                await next(context);
            }
        }
    }
    
    // public class WeatherMiddleware
    // {
    //     private readonly RequestDelegate next;
    //     private readonly IResponseFormatter responseFormatter;
    //
    //     public WeatherMiddleware(RequestDelegate nextDelegate, IResponseFormatter responseFormatter)
    //     {
    //         next = nextDelegate;
    //         this.responseFormatter = responseFormatter;
    //     }
    //
    //     public async Task Invoke(HttpContext context)
    //     {
    //         if (context.Request.Path.Value == "/middleware/class")
    //         {
    //             await responseFormatter.Format(context, "Middleware Class: It is raining in London");
    //         }
    //         else
    //         {
    //             await next(context);
    //         }
    //     }
    // }
}
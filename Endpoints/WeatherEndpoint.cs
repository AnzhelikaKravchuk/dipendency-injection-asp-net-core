﻿using System.Threading.Tasks;
using DependencyInjection.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace DependencyInjection.Endpoints
{
    public class WeatherEndpoint
    {
        public static async Task Endpoint(HttpContext context)
        {
            await context.Response.WriteAsync("Endpoint Class: It is cloudy in Milan");
        }
    
        // public static async Task Endpoint(HttpContext context)
        // { 
        //     IResponseFormatter formatter = context.RequestServices.GetRequiredService<IResponseFormatter>();
        //     await formatter.Format(context, "Endpoint Class: It is cloudy in Milan");
        // }
    }

    // public class WeatherEndpoint
    // {
    //     private IResponseFormatter formatter;
    //     
    //     public WeatherEndpoint(IResponseFormatter responseFormatter)
    //     {
    //         formatter = responseFormatter;
    //     }
    //     public async Task Endpoint(HttpContext context)
    //     {
    //         await formatter.Format(context, "Endpoint Class: It is cloudy in Milan");
    //     }
    // }
}
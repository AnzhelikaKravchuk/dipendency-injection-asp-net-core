﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DependencyInjection.Services.Interfaces
{
    public interface IResponseFormatter
    {
        Task Format(HttpContext context, string content);
        
        //public bool RichOutput => false;
    }
}
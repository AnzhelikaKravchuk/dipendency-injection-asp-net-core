﻿namespace DependencyInjection.Services.Interfaces
{
    public interface ITimeStamper
    {
        string TimeStamp { get; }
    }
}

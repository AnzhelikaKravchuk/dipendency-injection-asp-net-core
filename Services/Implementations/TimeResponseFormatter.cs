﻿using System.Threading.Tasks;
using DependencyInjection.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace DependencyInjection.Services.Implementations
{
    public class TimeResponseFormatter : IResponseFormatter
    {
        private ITimeStamper stamper;
        
        public TimeResponseFormatter(ITimeStamper timeStamper)
        {
            stamper = timeStamper;
        }
        public async Task Format(HttpContext context, string content)
        {
            await context.Response.WriteAsync($"{stamper.TimeStamp}: {content}");
        }
    }
}

﻿using System.Threading.Tasks;
using DependencyInjection.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace DependencyInjection.Services.Implementations
{
    public class TextResponseFormatter : IResponseFormatter
    {
        private int responseCounter = 0;

        private static TextResponseFormatter shared;

        public TextResponseFormatter() { }

        public async Task Format(HttpContext context, string content)
        {
            await context.Response.WriteAsync($"Response {++responseCounter}:\n{content}");
        }

        public static TextResponseFormatter Singleton => shared ??= new TextResponseFormatter();
    }
}
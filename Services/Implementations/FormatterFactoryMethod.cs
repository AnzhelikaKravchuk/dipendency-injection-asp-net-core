﻿using DependencyInjection.Services.Interfaces;

namespace DependencyInjection.Services.Implementations
{
    public static class FormatterFactoryMethod
    {
        //private static IResponseFormatter formatter = new TextResponseFormatter();

        private static IResponseFormatter formatter = new HtmlResponseFormatter();

        public static IResponseFormatter Formatter => formatter;

    }
}

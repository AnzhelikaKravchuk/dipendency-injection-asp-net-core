﻿using System;
using DependencyInjection.Services.Interfaces;

namespace DependencyInjection.Services.Implementations
{
    public class DefaultTimeStamper : ITimeStamper
    {
        public string TimeStamp => DateTime.Now.ToLongTimeString();
    }
}
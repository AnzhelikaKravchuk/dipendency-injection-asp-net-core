﻿using System;
using System.Threading.Tasks;
using DependencyInjection.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace DependencyInjection.Services.Implementations
{
    public class GuidService : IResponseFormatter
    {
        private Guid guid = Guid.NewGuid();
        public async Task Format(HttpContext context, string content)
        {
            await context.Response.WriteAsync($"Guid: {guid}\n{content}");
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DependencyInjection.Endpoints;
using DependencyInjection.Middleware;
using DependencyInjection.Services;
using DependencyInjection.Services.Implementations;
using DependencyInjection.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Routing;

namespace DependencyInjection
{
    public class Startup
    {
        private IServiceCollection services;

        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSingleton<IResponseFormatter, HtmlResponseFormatter>();

            //services.AddSingleton<IResponseFormatter, GuidService>();

            //services.AddTransient<IResponseFormatter, GuidService>();

            //services.AddScoped<IResponseFormatter, GuidService>();

            //services.AddScoped<IResponseFormatter, TimeResponseFormatter>();
            //services.AddScoped<ITimeStamper, DefaultTimeStamper>();

            // services.AddScoped<IResponseFormatter, TextResponseFormatter>();
            // services.AddScoped<IResponseFormatter, HtmlResponseFormatter>();
            // services.AddScoped<IResponseFormatter, GuidService>();
            
            // services.AddSingleton(typeof(ICollection<>), typeof(List<>));

            this.services = services;
        }


        #region ex.1.

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
        
            app.UseRouting();
        
            app.UseMiddleware<WeatherMiddleware>();
        
            IResponseFormatter formatter = new TextResponseFormatter();
        
            app.Use(async (context, next) =>
            {
                if (context.Request.Path.Value == "/middleware/function")
                {
                    await context.Response.WriteAsync("Middleware Function: It is snowing in Chicago");
                    //await formatter.Format(context, "Middleware Function: It is snowing in Chicago");
                }
                else
                {
                    await next();
                }
            });
        
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/endpoint/class", WeatherEndpoint.Endpoint);
        
                endpoints.MapGet("/endpoint/function",
                    async context =>
                    {
                        await context.Response.WriteAsync("Endpoint Function: It is sunny in LA");
                        //await formatter.Format(context, "Endpoint Function: It is sunny in LA");
                    });
                
            });
        
            app.Run(async context => await context.Response.WriteAsync("Terminal Middleware."));
        }

        #endregion


        #region ex.2.

        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        // {
        //     app.UseDeveloperExceptionPage();
        //
        //     app.UseRouting();
        //
        //     app.UseMiddleware<WeatherMiddleware>();
        //
        //     app.Use(async (context, next) =>
        //     {
        //         if (context.Request.Path.Value == "/middleware/function")
        //         {
        //             await TextResponseFormatter.Singleton.Format(context, "Middleware Function: It is snowing in Chicago");
        //         }
        //         else
        //         {
        //             await next();
        //         }
        //     });
        //
        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapGet("/endpoint/class", WeatherEndpoint.Endpoint);
        //
        //         endpoints.MapGet("/endpoint/function",
        //             async context =>
        //             {
        //                 await TextResponseFormatter.Singleton.Format(context, "Endpoint Function: It is sunny in LA");
        //             });
        //     });
        //
        //     app.Run(async context => await context.Response.WriteAsync("Terminal Middleware."));
        // }

        #endregion


        #region ex.3.

        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        // {
        //     app.UseDeveloperExceptionPage();
        //
        //     app.UseRouting();
        //
        //     app.UseMiddleware<WeatherMiddleware>();
        //
        //     app.Use(async (context, next) =>
        //     {
        //         if (context.Request.Path.Value == "/middleware/function")
        //         {
        //             await TypeBroker.Formatter.Format(context, "Middleware Function: It is snowing in Chicago");
        //         }
        //         else
        //         {
        //             await next();
        //         }
        //     });
        //
        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapGet("/endpoint/class", WeatherEndpoint.Endpoint);
        //
        //         endpoints.MapGet("/endpoint/function",
        //             async context =>
        //             {
        //                 await TypeBroker.Formatter.Format(context, "Endpoint Function: It is sunny in LA");
        //             });
        //     });
        //
        //     app.Run(async context => await context.Response.WriteAsync("Terminal Middleware."));
        // }

        #endregion
        
        #region ex.4.

        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IResponseFormatter formatter)
        // {
        //     app.UseDeveloperExceptionPage();
        //
        //     app.UseRouting();
        //
        //     app.UseMiddleware<WeatherMiddleware>();
        //
        //     app.Use(async (context, next) =>
        //     {
        //         if (context.Request.Path.Value == "/middleware/function")
        //         {
        //             await formatter.Format(context, "Middleware Function: It is snowing in Chicago");
        //         }
        //         else
        //         {
        //             await next();
        //         }
        //     });
        //
        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapGet("/endpoint/class", WeatherEndpoint.Endpoint);
        //
        //         //endpoints.MapWeather("/endpoint/class");
        //
        //         endpoints.MapGet("/endpoint/function",
        //             async context =>
        //             {
        //                 await formatter.Format(context, "Endpoint Function: It is sunny in LA");
        //             });
        //     });
        //
        //     app.Run(async context => await context.Response.WriteAsync("Terminal Middleware."));
        // }

        #endregion


        #region ex.5.(use services)

        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IResponseFormatter formatter)
        // {
        //     app.UseDeveloperExceptionPage();
        //
        //     app.UseRouting();
        //
        //     app.UseMiddleware<WeatherMiddleware>();
        //
        //     app.Use(async (context, next) =>
        //     {
        //         if (context.Request.Path.Value == "/middleware/function")
        //         {
        //             await formatter.Format(context, "Middleware Function: It is snowing in Chicago");
        //         }
        //         else
        //         {
        //             await next();
        //         }
        //     });
        //
        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapGet("/endpoint/class", WeatherEndpoint.Endpoint);
        //
        //         endpoints.MapGet("/endpoint/function",
        //             async context =>
        //             {
        //                 await formatter.Format(context, "Endpoint Function: It is sunny in LA");
        //             });
        //     });
        //
        //     app.Run(async context => await context.Response.WriteAsync("Terminal Middleware."));
        // }

        #endregion


        #region ex.6.(use services)

        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IResponseFormatter formatter)
        // {
        //     app.UseDeveloperExceptionPage();
        //
        //     app.UseRouting();
        //
        //     app.UseMiddleware<WeatherMiddleware>();
        //
        //     app.Use(async (context, next) =>
        //     {
        //         if (context.Request.Path.Value == "/middleware/function")
        //         {
        //             await formatter.Format(context, "Middleware Function: It is snowing in Chicago");
        //         }
        //         else
        //         {
        //             await next();
        //         }
        //     });
        //
        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapWeather("/endpoint/class");
        //
        //         endpoints.MapGet("/endpoint/function",
        //             async context =>
        //             {
        //                 await formatter.Format(context, "Endpoint Function: It is sunny in LA");
        //             });
        //     });
        //
        //     app.Run(async context => await context.Response.WriteAsync("Terminal Middleware."));
        // }

        #endregion


        #region ex.7.

        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        // {
        //     // app.Run(async context =>
        //     // {
        //     //     var sb = new StringBuilder();
        //     //     sb.Append("<h1>Services</h1>");
        //     //     sb.Append("<table>");
        //     //     sb.Append("<tr><th>Тип</th><th>Lifetime</th><th>Implementation</th></tr>");
        //     //     foreach (var svc in services)
        //     //     {
        //     //         sb.Append("<tr>");
        //     //         sb.Append($"<td>{svc.ServiceType.FullName}</td>");
        //     //         sb.Append($"<td>{svc.Lifetime}</td>");
        //     //         sb.Append($"<td>{svc.ImplementationType?.FullName}</td>");
        //     //         sb.Append("</tr>");
        //     //     }
        //     //
        //     //     sb.Append("</table>");
        //     //     context.Response.ContentType = "text/html;charset=utf-8";
        //     //     await context.Response.WriteAsync(sb.ToString());
        //     // });
        //
        //     app.UseDeveloperExceptionPage();
        //
        //     app.UseRouting();
        //
        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapGet("/single", async context =>
        //         {
        //             IResponseFormatter formatter = context.RequestServices
        //                 .GetService<IResponseFormatter>();
        //                 //.GetServices<IResponseFormatter>().First(p => p is HtmlResponseFormatter);
        //             await formatter.Format(context, "Single service");
        //         });
        //
        //         endpoints.MapGet("/multiple", async context =>
        //         {
        //             IResponseFormatter formatter = context.RequestServices
        //                 .GetService<IResponseFormatter>();
        //                 //.GetServices<IResponseFormatter>().First(f => f.RichOutput);
        //             await formatter.Format(context, "Multiple services");
        //         });
        //     });
        // }

        #endregion

        
        #region ex.8.

        // public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        // {
        //     app.UseDeveloperExceptionPage();
        //
        //     app.UseRouting();
        //
        //     app.UseEndpoints(endpoints =>
        //     {
        //         endpoints.MapGet("/string", async context =>
        //         {
        //             ICollection<string> collection = context.RequestServices.GetService<ICollection<string>>();
        //             collection.Add($"Request: { DateTime.Now.ToLongTimeString() }");
        //             foreach (string str in collection)
        //             {
        //                 await context.Response.WriteAsync($"String: {str}\n");
        //             }
        //         });
        //
        //         endpoints.MapGet("/int", async context =>
        //         {
        //             ICollection<int> collection = context.RequestServices.GetService<ICollection<int>>();
        //             collection.Add(collection.Count() + 1);
        //             foreach (int val in collection)
        //             {
        //                 await context.Response.WriteAsync($"Int: {val}\n");
        //             }
        //         });
        //     });
        // } 

        #endregion
    }
}